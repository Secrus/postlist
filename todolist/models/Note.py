from django.db import models
from django.utils import timezone


class Note(models.Model):
    STATUS_CHOICES = (
        ("Finished", "Finished"),
        ("Pending", "Pending"),
    )

    content = models.TextField()
    creation_date = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=1,
                              choices=STATUS_CHOICES,
                              default="Pending")
    end_date = models.DateTimeField(blank=True, null=True)
    owner = models.ForeignKey('auth.User',
                              related_name='todolist',
                              on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        super(Note, self).save(*args, **kwargs)

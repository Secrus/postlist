from rest_framework import serializers
from todolist.models import Note


class NoteSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Note
        fields = ('content', 'status', 'creation_date', 'owner')

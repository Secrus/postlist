from todolist.models import Note
from todolist.serializers import NoteSerializer
from rest_framework import permissions
from todolist.permissions import IsOwnerOrReadOnly
from rest_framework import viewsets


class NoteViewSet(viewsets.ModelViewSet):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

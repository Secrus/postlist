from django.urls import path, include
from rest_framework.routers import DefaultRouter
from todolist import views


router = DefaultRouter()
router.register(r'notes', views.NoteViewSet)
router.register(r'users', views.UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
]

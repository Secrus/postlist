# PostList - my Todo list application

![https://img.shields.io/pypi/pyversions/django](https://img.shields.io/badge/Python-3.6%2B-blue)
![https://www.djangoproject.com/](https://img.shields.io/badge/Django-2.2.2-green)

### TODO
 - JWT authentication
 - unit tests
 - further development


## About the project
This application was created as my training in Django REST Framework. The intent of this project was to learn how to create DRF application and develop it. In the future I plan to further develop app, add features like nested lists, tasks sharing, task scheduling for exact date and/or hour and mail notifications about acheduled tasks.